'use strict';

angular.module('tbucheaApp')

  .controller('AboutCtrl', function($scope) {
    $scope.avatars = [
      {'icon': '&#xf09b;', 'bg': 'github.jpg', 'href': 'https://github.com/tylerbuchea'},
      {'icon': '&#xf0d4;', 'bg': 'gplus.jpg', 'href': 'https://plus.google.com/115309649913679483813'},
      {'icon': '&#xf081;', 'bg': 'twitter.jpg', 'href': 'https://twitter.com/tylerbuchea'},
      {'icon': '&#xf082;', 'bg': 'fb.jpg', 'href': 'https://www.facebook.com/tyler.buchea'}
    ];
  })

  .controller('ProductsCtrl', function ($scope) {
    $scope.products = [
      'Facebook #2',
      'Google But Better',
      'Tylerwitter'
    ];
  })

  .controller('ProjectsCtrl', function ($scope) {
    $scope.projects = [
      {'title': 'Go Tell That', 'href': 'http://gotellthat.com/', 'bg': 'gotellthat.png'},
      {'title': 'Lure.js', 'href': 'http://tylerbuchea.com/projects/lure', 'bg': 'lure.png'},
      {'title': 'Git Commands', 'href': 'http://tylerbuchea.com/projects/git-commands', 'bg': 'git-commands.png'},
      {'title': 'Snakes', 'href': 'http://tylerbuchea.com/projects/snakes', 'bg': 'snake.png'},
      {'title': 'Fios WEP Calculator', 'href': 'http://tylerbuchea.com/projects/fiwepc', 'bg': 'fiwepc.png'},
      {'title': 'Marklets', 'href': 'http://tylerbuchea.com/projects/marklets', 'bg': 'marklets.png'}
    ];
  })

  .controller('SnippetsCtrl', function ($scope, $http) {
    var url = 'https://api.github.com/users/tylerbuchea/gists?callback=JSON_CALLBACK';

    $http.jsonp(url).
      success(function(data) {
        $scope.snippets = data.data;
      }).
      error(function() {
        $scope.error = true;
      });

  });