'use strict';

angular.module('tbucheaApp', ['ngSanitize'])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        redirectTo: '/projects'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/products', {
        templateUrl: 'views/products.html',
        controller: 'ProductsCtrl'
      })
      .when('/projects', {
        templateUrl: 'views/projects.html',
        controller: 'ProjectsCtrl'
      })
      .when('/snippets', {
        templateUrl: 'views/snippets.html',
        controller: 'SnippetsCtrl'
      })
      .otherwise({
        redirectTo: '/projects'
      });
  });
